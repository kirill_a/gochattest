// Copyright 2013 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"flag"
	"html/template"
	"log"
    "sync"
	"net/http"

	"github.com/gorilla/websocket"
    "container/list"
)

var addr = flag.String("addr", "0.0.0.0:80", "http service address")

var upgrader = websocket.Upgrader{} // use default options
var mutex = &sync.Mutex{}

var connectionList = list.New();

func mainHandler(c *websocket.Conn) {
    defer c.Close()
	for {
		mt, message, err := c.ReadMessage()
		if err != nil {
			log.Println("read:", err)
			break
		} else {
            log.Printf("recv: %s", message)
            for e := connectionList.Front(); e != nil; e = e.Next() {
                // do something with e.Value
                
                cc := e.Value.(* websocket.Conn)
                if (cc!= nil){
                    writeInSocket(mt, message, cc)
                }
            }
        }
	}
}

func writeInSocket(messageType int, message []byte, c *websocket.Conn) {
            mutex.Lock()
            err := c.WriteMessage(messageType, message)
            if err != nil {
                log.Println("write:", err)
            }
            mutex.Unlock()
}

func echo(w http.ResponseWriter, r *http.Request) {
    c, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Print("upgrade:", err)
		return
	} else {
        connectionList.PushBack(c)
    }
    go mainHandler(c)
}

func home(w http.ResponseWriter, r *http.Request) {
	var homeTemplate = template.Must(template.New("test.html").ParseFiles("/go/src/goproxy/test.html"))
	homeTemplate.Execute(w, "ws://"+r.Host+"/chat")
}

func main() {
	flag.Parse()
	log.SetFlags(0)
	log.Println("setup handlers...")
	http.HandleFunc("/chat", echo)
	http.HandleFunc("/", home)
	log.Fatal(http.ListenAndServe(*addr, nil))
}

